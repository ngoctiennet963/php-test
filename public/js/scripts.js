$(document).ready(function () {
    function initDatepickerFrom() {
        $('.from').datepicker({
            autoclose: true,
            clearBtn: true,
            format: 'yyyy-mm-dd',
            orientation: "bottom auto",
        }).on('changeDate', function (selected) {
            startDate = new Date(selected.date.valueOf());
            startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
            $('.to').datepicker('setStartDate', startDate);
        }).on('show.bs.modal', function (event) {
            event.stopPropagation();
        });
    }
    
    function initDatepickerTo() {
        $('.to').datepicker({
            autoclose: true,
            clearBtn: true,
            format: 'yyyy-mm-dd',
            orientation: 'bottom auto',
        }).on('changeDate', function (selected) {
            FromEndDate = new Date(selected.date.valueOf());
            FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
            $('.from').datepicker('setEndDate', FromEndDate);
        }).on('show.bs.modal', function (event) {
            event.stopPropagation();
        });
    }

    initDatepickerFrom();
    initDatepickerTo();
});

