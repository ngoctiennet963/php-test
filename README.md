﻿
  

# ROUND 1: TEST TOPIC

This is basic one to check coding behavior, style and basic technical before interview round. Thank for your cooperation.

## CONTENT:

You need a web system to manage your work named “TodoList” with the below functions:

##### 1. Functions to Add, Edit, Delete Work. A work includes information of “Work Name”, “Starting Date”, “Ending Date” and “Status” (Planning, Doing, Complete)

##### 2. Function to show the works on a calendar view with: Date, Week, Month (can use third-party library)

  

## REQUEST:

##### 1. To do with PHP programming language follows MVC. PLEASE DO NOT USE ANY FRAMEWORK

##### 2. To use coding convention follows PSR

##### 3. To apply UNIT TEST to test your functions

##### 4. To use GIT and GIT Flow to manage your code

##### 5. Please do your best, no need to complete all contents if you cannot

##### 6. Please send your result to board@tech.est-rouge.com


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

Init Composer
```
php composer install
```

Init phinx for migrations
```
vendor/bin/phinx init
```

Config phinx enviroment (optional), default development enviroment
```
vendor/bin/phinx -e development
```

Create migration
```
composer migrate
```

Seed data migration
```
composer migration:seed
```

Check coding conventions
```
composer lint
```

Fix automation coding conventions
```
composer lint-fix
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

```
composer test
```

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system