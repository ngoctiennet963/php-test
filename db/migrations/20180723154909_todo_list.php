﻿<?php

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class TodoList extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('todo_list');
        $table->addColumn('work_name', 'string', ['limit' => 255])
            ->addColumn('start_date', 'date')
            ->addColumn('end_date', 'date')
            ->addColumn('status', 'integer', ['limit' => MysqlAdapter::INT_SMALL, 'default' => 1, 'comment' => '1: Planning, 2: Doing, 3: Complete'])
            ->addColumn('created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'datetime', ['null' => true])
            ->addColumn('deleted_at', 'datetime', ['null' => true])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->table('todo_list')->drop()->save();
    }
}
