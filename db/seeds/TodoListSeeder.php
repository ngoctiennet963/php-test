<?php

use Phinx\Seed\AbstractSeed;

class TodoListSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $data = [];
        for ($i = 0; $i < 10; $i++) {
            $randInt    = rand(1, 3);
            $randomDate = $faker->dateTimeBetween('-30 days', 'now');
            $start_date = clone $randomDate;
            $end_date   = $randomDate->modify("{$randInt} days");

            $data[] = [
                'work_name'  => $faker->realText(rand(10, 20)),
                'start_date' => $start_date->format('Y-m-d'),
                'end_date'   => $end_date->format('Y-m-d'),
                'status'     => $faker->numberBetween(1, 3),
                'created_at' => date('Y-m-d H:i:s'),
            ];
        }

        $this->table('todo_list')->insert($data)->save();
    }
}
