<?php

namespace Tests;

use app\core\DB;
use PHPUnit\Framework\TestCase;

class BaseTestCase extends TestCase
{
    public static function setUpBeforeClass()
    {
        $conn = DB::getInstance();
        $stmt = $conn->query("SHOW TABLES");
        foreach ($stmt->fetchAll() as $row) {
            $table = $row->{'Tables_in_' . DB_NAME};
            // Ignore table migration phinx
            if ($table !== 'phinxlog') {
                $conn->query("TRUNCATE TABLE `$table`");
            }
        }
    }

    public function tearDown()
    {
    }
}
