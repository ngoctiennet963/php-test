<?php

/*
|--------------------------------------------------------------------------
| Load config, autoloader class
|--------------------------------------------------------------------------
 */
require_once dirname(__DIR__) . '/config/test.php';
require_once dirname(__DIR__) . '/vendor/autoload.php';
