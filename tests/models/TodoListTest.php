<?php
namespace Tests;

use app\models\TodoList;
use Faker;
use Tests\BaseTestCase;

class TodoListTest extends BaseTestCase
{
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        $faker = Faker\Factory::create();

        // Faker random data
        for ($i = 0; $i < 4; $i++) {
            $row = new TodoList();
            $randInt = rand(1, 3);
            $randomDate = $faker->dateTimeBetween('-30 days', '-5 days');
            $start_date = clone $randomDate;
            $end_date = $randomDate->modify("{$randInt} days");

            $row = new TodoList();
            $row->work_name = $faker->realText(rand(10, 20));
            $row->start_date = $start_date->format('Y-m-d');
            $row->end_date = $end_date->format('Y-m-d');
            $row->status = $faker->numberBetween(1, 3);
            $row->save();
        }

        // Faker start_date = now
        $row = new TodoList();
        $row->work_name = $faker->realText(rand(10, 20));
        $row->start_date = date('Y-m-d');
        $row->end_date = date('Y-m-d');
        $row->status = $faker->numberBetween(1, 3);
        $row->save();
    }

    public function testFind()
    {
        $todo = new TodoList();
        $todo = $todo->find();
        $this->assertCount(5, $todo);
    }

    public function testFindToday()
    {
        $todo = new TodoList();
        $todo = $todo->find(['start_date' => date('Y-m-d')]);
        $this->assertCount(1, $todo);
        $todo = current($todo);
        $this->assertEquals($todo->start_date, date('Y-m-d'));
    }

    public function testCount()
    {
        $todo = new TodoList();
        $todo = $todo->count();
        $this->assertEquals(5, $todo);
    }

    public function testFindOne()
    {
        $todo = new TodoList();
        $todo = $todo->findOne(['id' => 1]);
        $this->assertEquals(1, $todo->id);
    }

    public function testAttributeModel()
    {
        $todo = new TodoList();
        $todo = $todo->findOne(['id' => 1]);
        $this->assertObjectHasAttribute('id', $todo);
        $this->assertObjectHasAttribute('work_name', $todo);
        $this->assertObjectHasAttribute('start_date', $todo);
        $this->assertObjectHasAttribute('end_date', $todo);
        $this->assertObjectHasAttribute('status', $todo);
        $this->assertObjectHasAttribute('created_at', $todo);

        $this->assertObjectNotHasAttribute('name', $todo);
        $this->assertObjectNotHasAttribute('content', $todo);
    }

    public function testUpdate()
    {
        $id = 1;
        $row = new TodoList();
        $row->id = $id;
        $row->work_name = "Truong Ngoc Tien";
        $row->start_date = date('Y-m-01');
        $row->end_date = date('Y-m-d');
        $row->status = 3;
        $row->save();

        $todo = new TodoList();
        $todo = $todo->findOne(['id' => $id]);
        $this->assertEquals("Truong Ngoc Tien", $todo->work_name);
        $this->assertEquals(date('Y-m-01'), $todo->start_date);
        $this->assertEquals(date('Y-m-d'), $todo->end_date);
        $this->assertEquals(3, $todo->status);
    }

    public function testDelete()
    {
        $id = 1;
        $row = new TodoList();
        $row->id = $id;
        $row->delete();

        $todo = new TodoList();
        $todo = $todo->findOne(['id' => $id]);

        $this->assertEmpty($todo);
    }

    public function testStatusName()
    {
        $status1 = TodoList::getStatusName(1);
        $status2 = TodoList::getStatusName(2);
        $status3 = TodoList::getStatusName(3);

        $this->assertEquals("Planning", $status1);
        $this->assertEquals("Doing", $status2);
        $this->assertEquals("Complete", $status3);
    }

}
