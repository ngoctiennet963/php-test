<?php

define('PATH_SYSTEM', dirname(__DIR__));
define('PATH_APPLICATION', dirname(__DIR__) . '/app');
define('DB_HOST', 'localhost');
define('DB_PORT', 3306);
define('DB_USER', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'development_db');
define('ASSETS_VERSION', '1.0');

return [
    'DEFAULT_CONTROLLER' => 'TodoList',
    'DEFAULT_ACTION' => 'index',
    '404_CONTROLLER' => 'error',
    '404_ACTION' => 'index',
];
