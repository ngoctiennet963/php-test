<?php

/*
|--------------------------------------------------------------------------
| Class Application
|--------------------------------------------------------------------------
 */
class Application
{
    private $config;

    public function __construct($config)
    {
        session_start();
        $this->config = $config;
    }

    public function run()
    {
        // Get default controller if not exists name controller in url
        $controller = empty($_GET['c']) ? $this->config['DEFAULT_CONTROLLER'] : $_GET['c'];
        $controller .= 'Controller';

        // Get default controller if not exists name action in url
        $action = empty($_GET['action']) ? $this->config['DEFAULT_ACTION'] : $_GET['action'];
        $action = 'action' . ucfirst($action);

        $controllerObject = '\\app\\controllers\\' . $controller;
        if (!class_exists($controllerObject)) {
            die("Controller cannot be found");
        }
        $controllerObject = new $controllerObject;

        // Check action exists in controller
        if (!method_exists($controllerObject, $action)) {
            die("Action cannot be found");
        }

        // Run
        $controllerObject->{$action}();
    }
}
