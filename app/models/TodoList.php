<?php

namespace app\models;

use app\core\DB;
use app\models\BaseModel;
use PDO;

class TodoList extends BaseModel
{
    /**
     * Define table name of Model
     *
     * @author TienTN <ngoctiennet963@gmail.com>
     */
    private static $table = 'todo_list';

    public $id;
    public $work_name;
    public $start_date;
    public $end_date;
    public $status;

    const STATUS_PLANNING = 1;
    const STATUS_DOING = 2;
    const STATUS_COMPLETE = 3;

    public function __construct()
    {
        parent::__construct();
    }

    public function find($where = [], $orderBy = ['id' => 'DESC'], $page = 1, $limit = 10)
    {
        // where
        $whereStr = '';
        if (!empty($where)) {
            $whereStr = ' WHERE';
            foreach ($where as $k => $v) {
                $whereStr .= " {$k} = :{$k} AND";
            }
            $whereStr = substr($whereStr, 0, -4);
        }

        // order by
        $orderKey = key($orderBy);
        $orderValue = $orderBy[key($orderBy)];

        // pagination
        $offset = $limit * ($page - 1);

        // Prepare sql string
        $sql = "SELECT * FROM " . self::$table;
        $sql .= $whereStr;
        $sql .= " ORDER BY {$orderKey} {$orderValue}";
        $sql .= " LIMIT {$limit} OFFSET {$offset}";

        $stmt = DB::getInstance()->prepare($sql);
        if (!empty($where)) {
            foreach ($where as $k => $v) {
                $stmt->bindParam(":{$k}", $v);
            }
        }
        $stmt->execute();
        $resultSet = $stmt->fetchAll();

        return $resultSet;
    }

    public function findOne($where = [])
    {
        $result = $this->find($where);
        if ($result) {
            $result = current($result);
            $this->id = $result->id;
            return $result;
        }
        return [];
    }

    public function count($where = [])
    {
        // where
        $whereStr = '';
        if (!empty($where)) {
            $whereStr = ' WHERE';
            foreach ($where as $k => $v) {
                $whereStr .= " {$k} = :{$k} AND";
            }
            $whereStr = substr($whereStr, 0, -4);
        }

        // Prepare sql string
        $sql = "SELECT count(0) FROM " . self::$table;
        $sql .= $whereStr;

        $stmt = DB::getInstance()->prepare($sql);
        if (!empty($where)) {
            foreach ($where as $k => $v) {
                $stmt->bindParam(":{$k}", $v);
            }
        }
        $stmt->execute();
        $count = $stmt->fetchColumn();

        return $count;
    }

    public function save()
    {
        try {
            if ($this->id) {
                $stmt = DB::getInstance()->prepare("UPDATE todo_list SET `work_name` = ?, `start_date` = ?, `end_date` = ?, `status` = ? WHERE id = ?");
                $stmt->bindParam(1, $this->work_name, PDO::PARAM_STR);
                $stmt->bindParam(2, $this->start_date, PDO::PARAM_STR);
                $stmt->bindParam(3, $this->end_date, PDO::PARAM_STR);
                $stmt->bindParam(4, $this->status, PDO::PARAM_INT);
                $stmt->bindParam(5, $this->id, PDO::PARAM_INT);
            } else {
                $stmt = DB::getInstance()->prepare("INSERT INTO todo_list(`work_name`, `start_date`, `end_date`, `status`) VALUES (?, ?, ?, ?)");
                $stmt->bindParam(1, $this->work_name, PDO::PARAM_STR);
                $stmt->bindParam(2, $this->start_date, PDO::PARAM_STR);
                $stmt->bindParam(3, $this->end_date, PDO::PARAM_STR);
                $stmt->bindParam(4, $this->status, PDO::PARAM_INT);
            }
            return $stmt->execute();
        } catch (PDOExecption $e) {
            die($e->getMessage());
        }
    }

    public function delete()
    {
        try {
            $stmt = DB::getInstance()->prepare("DELETE FROM todo_list WHERE id = ?");
            $stmt->bindParam(1, $this->id, PDO::PARAM_STR);
            return $stmt->execute();
        } catch (PDOExecption $e) {
            die($e->getMessage());
        }
    }

    public function getStatusName($statusId)
    {
        switch ($statusId) {
            case self::STATUS_PLANNING:
                return 'Planning';
            case self::STATUS_DOING:
                return 'Doing';
            default:
                return 'Complete';
        }
    }
}
