<header>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">
            <img src="/public/img/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
            Bootstrap v4.0.0
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <?=$_SERVER['REQUEST_URI'] == '/' ? 'active' : ''?>">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item <?=$_SERVER['REQUEST_URI'] == '/?action=list' ? 'active' : ''?>">
                    <a class="nav-link" href="/?action=list">CURD TodoList</a>
                </li>
            </ul>
        </div>
    </nav>
</header>