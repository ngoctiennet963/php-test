<?php if (isset($_SESSION['alert_success'])) {
    $message = $_SESSION['alert_success'];
    unset($_SESSION['alert_success']);
?>
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong><?=$message?></strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php } else if (isset($_SESSION['alert_warning'])) {
    $message = $_SESSION['alert_warning'];
    unset($_SESSION['alert_warning']);
?>
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <strong><?=$message?></strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php } ?>