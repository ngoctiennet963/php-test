<?php
use app\models\TodoList;
?>
<a href="/?action=create" class="btn btn-success">Add new</a>
<hr>
<!-- BEGIN Alert Message -->
<?php $this->renderPatial('inc/alert_message'); ?>
<!-- END Alert Message -->

<?php if (!empty($data['data'])) { ?>
<table class="table table-striped table-hover mt-3">
    <caption>List of TodoList</caption>
    <thead class="thead-dark">
        <tr>
            <th>#</th>
            <th style="width: 25%">Work name</th>
            <th>Start date</th>
            <th>End date</th>
            <th>Status</th>
            <th>Created At</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data['data'] as $value) { ?>
        <tr>
            <td class="text-center"><?=$value->id?></td>
            <td><?=$value->work_name?></td>
            <td><?=$value->start_date?></td>
            <td><?=$value->end_date?></td>
            <td><?=TodoList::getStatusName($value->status)?></td>
            <td><?=$value->created_at?></td>
            <td class="text-center">
                <a href="/?action=update&id=<?=$value->id?>" class="btn btn-info">Update</a>
                <a href="/?action=delete&id=<?=$value->id?>" class="btn btn-danger">Delete</a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<?php } ?>
