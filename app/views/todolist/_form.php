<h2>Add new TodoList</h3>
<hr>
<!-- BEGIN Alert Message -->
<?php $this->renderPatial('inc/alert_message'); ?>
<!-- END Alert Message -->

<?php $model = $data['model']; ?>
<form class="needs-validation" novalidate method="POST" action="">
    <div class="form-row">
        <div class="col-md-12 mb-3">
            <label for="validationCustom01">Work Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="validationCustom01"><i class="fas fa-tasks"></i></span>
                </div>
                <input type="text" name="work_name" class="form-control" id="validationCustom01" placeholder="Work Name" aria-describedby="validationCustom01" required value="<?=$model->work_name?>">
                <div class="invalid-feedback">Please choose a Work Name.</div>
                <div class="valid-feedback">Looks good!</div>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-4 mb-3">
            <label for="validationCustomStartDate">Start Date</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-calendar-alt"></i></span>
                </div>
                <input type="text" name="start_date" class="form-control from" id="validationCustomStartDate" placeholder="Start Date" aria-describedby="inputGroupPrepend" required value="<?=$model->start_date?>">
                <div class="invalid-feedback">Please choose a Start Date.</div>
                <div class="valid-feedback">Looks good!</div>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <label for="validationCustomEndDate">End Date</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupPrepend"><i class="fas fa-calendar-alt"></i></span>
                </div>
                <input type="text" name="end_date" class="form-control to" id="validationCustomEndDate" placeholder="End Date" aria-describedby="inputGroupPrepend" required value="<?=$model->end_date?>">
                <div class="invalid-feedback">Please choose a End Date.</div>
                <div class="valid-feedback">Looks good!</div>
            </div>
        </div>
        <div class="col-md-4 mb-3">
            <label for="validationCustomUsername">&nbsp;</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelect01">Status</label>
                </div>
                <select name="status" class="custom-select" id="inputGroupSelect01">
                    <option value="1" <?=$model->status==1?'selected':''?>>Planning</option>
                    <option value="2" <?=$model->status==2?'selected':''?>>Doing</option>
                    <option value="3" <?=$model->status==3?'selected':''?>>Complete</option>
                </select>
            </div>
        </div>
    </div>
    <button class="btn btn-primary" type="submit">Submit form</button>
</form>

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();
</script>
