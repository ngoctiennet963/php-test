
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PHP</title>

        <!-- Stylesheet CSS Files -->
        <link href="/public/plugin/bootstrap/css/bootstrap.min.css?v=<?=ASSETS_VERSION?>" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
        <link href="/public/plugin/bootstrap-datepicker/css/bootstrap-datepicker3.min.css?v=<?=ASSETS_VERSION?>" rel="stylesheet">
        <link href="/public/plugin/fullcalendar/css/fullcalendar.css?v=<?=ASSETS_VERSION?>" rel="stylesheet">
        <link href="/public/css/styles.css?v=<?=ASSETS_VERSION?>" rel="stylesheet">
    </head>
    <body>

        <!-- BEGIN navbar -->
        <?php $this->renderPatial('inc/navbar'); ?>
        <!-- END navbar -->

        <!-- BEGIN page content -->
        <main role="main" class="container mt-3">
            <?=$content?>
        </main>
        <!-- END page content -->

        <!-- BEGIN page footer -->
        <footer class="footer">
            <div class="container">
                <span class="text-muted">Place sticky footer content here.</span>
            </div>
        </footer>
        <!-- END page footer -->

        <!-- Stylesheet JS Files -->
        <script src="/public/js/jquery-3.3.1.min.js?v=<?=ASSETS_VERSION?>"></script>
        <script src="/public/js/jquery-ui.min.js?v=<?=ASSETS_VERSION?>"></script>
        <script src="/public/plugin/bootstrap/js/bootstrap.min.js?v=<?=ASSETS_VERSION?>"></script>
        <script src="/public/plugin/bootstrap-datepicker/js/bootstrap-datepicker.min.js?v=<?=ASSETS_VERSION?>"></script>
        <script src="/public/plugin/fullcalendar/js/fullcalendar.js?v=<?=ASSETS_VERSION?>"></script>
        <script src="/public/js/scripts.js?v=<?=ASSETS_VERSION?>"></script>
    </body>
</html>