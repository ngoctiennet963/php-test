<?php

namespace app\controllers;

use app\controllers\BaseController;
use app\models\TodoList;

class TodoListController extends BaseController
{

    /**
     * Init layout
     *
     * @author TienTN <ngoctiennet963@gmail.com>
     */
    public function __construct()
    {
        $this->setLayout('layouts/main');
        parent::__construct();
    }

    /**
     * Show calendar TodoList (use third-party library)
     *
     * @author TienTN <ngoctiennet963@gmail.com>
     */
    public function actionIndex()
    {
        $todoList = new TodoList();
        $data = $todoList->find([], ['id' => 'DESC'], 1, 9999);

        // Generation json struct data event for plugin fullcalendar
        $jsonEvent = [];
        if (!empty($data)) {
            foreach ($data as $v) {
                $status = TodoList::getStatusName($v->status);
                $jsonEvent[] = [
                    'title'     => $v->work_name . '(' . $status . ')',
                    'start'     => $v->start_date,
                    'end'       => $v->end_date,
                    'allDay'    => false,
                    'className' => strtolower($status),
                ];
            }
        }

        $jsonEvent = json_encode($jsonEvent);

        $this->render('index', ['data' => $jsonEvent]);
    }

    /**
     * Return index view table TodoList
     *
     * @author TienTN <ngoctiennet963@gmail.com>
     */
    public function actionList()
    {
        $todoList = new TodoList();
        $data = $todoList->find();

        $this->render('list', ['data' => $data]);
    }

    /**
     * Create new TodoList
     *
     * @author TienTN <ngoctiennet963@gmail.com>
     */
    public function actionCreate()
    {
        $todoList = new TodoList();
        if (isset($_POST) && !empty($_POST['work_name'])) {
            $todoList->work_name  = $_POST['work_name'];
            $todoList->start_date = $_POST['start_date'];
            $todoList->end_date   = $_POST['end_date'];
            $todoList->status     = $_POST['status'];
            if ($todoList->save()) {
                $_SESSION['alert_success'] = "Add new TodoList Successfuly.";
            } else {
                $_SESSION['alert_warning'] = "Please re-check input.";
            }

            self::redirectList();
        }

        $this->render('_form', ['model' => $todoList]);
    }

    /**
     * Update TodoList
     *
     * @param integer $id ID_TodoList
     * @author TienTN <ngoctiennet963@gmail.com>
     */
    public function actionUpdate()
    {
        if (isset($_GET) && !empty($_GET['id'])) {
            $id = (int) $_GET['id'];
            $todoList = new TodoList();
            $model = $todoList->findOne(['id' => $id]);
            if (!$model) {
                self::redirectList();
            }

            if (isset($_POST) && !empty($_POST['work_name'])) {
                $todoList->work_name  = $_POST['work_name'];
                $todoList->start_date = $_POST['start_date'];
                $todoList->end_date   = $_POST['end_date'];
                $todoList->status     = $_POST['status'];
                if ($todoList->save()) {
                    $_SESSION['alert_success'] = "Update TodoList Successfuly.";
                } else {
                    $_SESSION['alert_warning'] = "Please re-check input.";
                }

                self::redirectList();
            }

            $this->render('_form', ['model' => $model]);
        } else {
            self::redirectList();
        }
    }

    /**
     * Delete TodoList
     *
     * @param integer $id ID_TodoList
     * @author TienTN <ngoctiennet963@gmail.com>
     */
    public function actionDelete()
    {
        if (isset($_GET) && !empty($_GET['id'])) {
            $id = (int) $_GET['id'];
            $todoList = new TodoList();
            $model = $todoList->findOne(['id' => $id]);
            if (!$model) {
                self::redirectList();
            }

            if ($todoList->delete()) {
                $_SESSION['alert_success'] = "Delete TodoList Successfuly.";
            } else {
                $_SESSION['alert_warning'] = "Please re-check input.";
            }
        }

        self::redirectList();
    }

    private static function redirectList()
    {
        exit(header('Location: /?action=list'));
    }
}
