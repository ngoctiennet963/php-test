<?php

namespace app\controllers;

class BaseController
{
    private $config;
    private $layout = null;

    public function __construct()
    {
        $this->config['layout'] = 'layouts/main';
        $this->config['rootDir'] = PATH_APPLICATION;
    }

    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    public function render($view, $data = null)
    {
        $rootDir = $this->config['rootDir'];

        $content = $this->getViewContent($view, $data);
        if ($this->layout != null) {
            $layoutPath = $rootDir . '/views/' . $this->layout . '.php';
            if (file_exists($layoutPath)) {
                require $layoutPath;
            }
        }
    }

    public function getViewContent($view, $data = null)
    {
        $rootDir = $this->config['rootDir'];
        $controller = get_class($this);
        $controller = explode('\\', $controller);
        $controller = end($controller);
        $folderView = strtolower(str_replace('Controller', '', $controller));

        if (is_array($data)) {
            extract($data, EXTR_PREFIX_SAME, "data");
        } else {
            $data = $data;
        }

        $viewPath = $rootDir . '/views/' . $folderView . '/' . $view . '.php';
        if (file_exists($viewPath)) {
            ob_start();
            require $viewPath;
            return ob_get_clean();
        }
    }

    public function renderPatial($view, $data = null)
    {
        $rootDir = $this->config['rootDir'];

        if (is_array($data)) {
            extract($data, EXTR_PREFIX_SAME, "data");
        } else {
            $data = $data;
        }

        $viewPath = $rootDir . '/views/' . $view . '.php';
        if (file_exists($viewPath)) {
            require $viewPath;
        }
    }
}
